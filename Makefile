include /usr/share/selinux/devel/Makefile

define assign-transmission-ports
echo "Assigning transmission port..."
semanage port -m -t transmission_port_t -p tcp 9091
semanage port -m -t transmission_port_t -p tcp 51415
semanage port -m -t transmission_port_t -p udp 51415
semanage port -m -t transmission_ldp_port_t -p udp 6771
endef

define remove-transmission-ports
echo "Removing transmission port..."
semanage port -d -t transmission_port_t -p tcp 9091
semanage port -d -t transmission_port_t -p tcp 51415
semanage port -d -t transmission_port_t -p udp 51415
semanage port -m -t transmission_ldp_port_t -p udp 6771
endef

define relabel-transmission-files
echo "Relabeling files..."
restorecon -DR /var/lib/transmission
restorecon -DR /var/spool/transmission
restorecon /usr/bin/transmission-daemon
endef

.PHONY: install uninstall update

install:
	semodule -v -i transmission.pp
	$(assign-transmission-ports)
	$(relabel-transmission-files)

uninstall:
	semodule -v -r transmission
	$(remove-transmission-ports)
	$(relabel-transmission-files)	

update:
	semodule -v -i transmission.pp
	$(relabel-transmission-files)
	