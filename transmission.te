policy_module(transmission, 1.0.0)

require {
	type fs_t;
	class filesystem quotaget;
}

# Access policies
#

# The target domain
type transmission_t;

# Main entry point and executables
type transmission_exec_t;

# Daemon transition
init_daemon_domain(transmission_t, transmission_exec_t)

# Allow executing own executables
can_exec(transmission_t, transmission_exec_t)

# Define init script domain and transition
type transmission_initrc_exec_t;
init_script_file(transmission_initrc_exec_t)

# Create a domain for transmission config file and
# allow transmission_t to read it.
type transmission_etc_t;
files_config_file(transmission_etc_t)
manage_dirs_pattern(transmission_t, transmission_etc_t, transmission_etc_t)
manage_files_pattern(transmission_t, transmission_etc_t, transmission_etc_t)
mmap_read_files_pattern(transmission_t, transmission_etc_t, transmission_etc_t)


# Create a domain for transmission variable files
# and allow transmission_t to manage them.
type transmission_var_lib_t;
files_type(transmission_var_lib_t)
files_search_var_lib(transmission_t)
manage_dirs_pattern(transmission_t, transmission_var_lib_t, transmission_var_lib_t)
manage_files_pattern(transmission_t, transmission_var_lib_t, transmission_var_lib_t)
mmap_read_files_pattern(transmission_t, transmission_var_lib_t, transmission_var_lib_t)
manage_lnk_files_pattern(transmission_t, transmission_var_lib_t, transmission_var_lib_t)
files_var_lib_filetrans(transmission_t, transmission_var_lib_t, { dir })

# Create a domain for transmission log files
# and allow transmission_t to manage them.
type transmission_log_t;
logging_log_file(transmission_log_t)
manage_dirs_pattern(transmission_t, transmission_log_t, transmission_log_t)
manage_files_pattern(transmission_t, transmission_log_t, transmission_log_t)
manage_lnk_files_pattern(transmission_t, transmission_log_t, transmission_log_t)
logging_log_filetrans(transmission_t, transmission_log_t, { dir file lnk_file })

# Create a domain for transmission tmp files
# and allow transmission_t to manage them.
type transmission_tmp_t;
files_tmp_file(transmission_tmp_t)
manage_dirs_pattern(transmission_t, transmission_tmp_t, transmission_tmp_t)
manage_files_pattern(transmission_t, transmission_tmp_t, transmission_tmp_t)
manage_lnk_files_pattern(transmission_t, transmission_tmp_t, transmission_tmp_t)
files_tmp_filetrans(transmission_t, transmission_tmp_t, { dir file lnk_file })

# Create a type for transmission tmpfs files
# and allow transmission_t to manage them.
type transmission_tmpfs_t;
files_tmpfs_file(transmission_tmpfs_t)
manage_dirs_pattern(transmission_t, transmission_tmpfs_t, transmission_tmpfs_t)
manage_files_pattern(transmission_t, transmission_tmpfs_t, transmission_tmpfs_t)
mmap_read_files_pattern(transmission_t, transmission_tmpfs_t, transmission_tmpfs_t)
manage_lnk_files_pattern(transmission_t, transmission_tmpfs_t, transmission_tmpfs_t)
fs_tmpfs_filetrans(transmission_t, transmission_tmpfs_t, { dir file lnk_file })

# Create a type for transmission content files and allow it to manage them
type transmission_content_rw_t;
files_type(transmission_content_rw_t)
manage_dirs_pattern(transmission_t, transmission_content_rw_t, transmission_content_rw_t)
manage_files_pattern(transmission_t, transmission_content_rw_t, transmission_content_rw_t)

# Define transmission port
type transmission_port_t;
corenet_port(transmission_port_t)

allow transmission_t transmission_port_t:tcp_socket { name_bind name_connect };

# Define transmission local peer discovery port
type transmission_ldp_port_t;
corenet_port(transmission_ldp_port_t)

# Allow access to http/https port
corenet_tcp_connect_http_port(transmission_t)

# Allow transmission to connect using any TCP port.
corenet_sendrecv_all_client_packets(transmission_t)
corenet_tcp_connect_all_ports(transmission_t)


allow transmission_t self:udp_socket create_socket_perms;
allow transmission_t self:tcp_socket create_stream_socket_perms;
allow transmission_t self:unix_dgram_socket { create_socket_perms sendto };
allow transmission_t self:netlink_route_socket r_netlink_socket_perms;

# Allow transmission to access all interfaces and
# nodes (IP Addresses)
corenet_tcp_sendrecv_all_if(transmission_t)
corenet_udp_sendrecv_all_if(transmission_t)
corenet_tcp_sendrecv_all_nodes(transmission_t)
corenet_udp_sendrecv_all_nodes(transmission_t)

#
# System and Process stuff
#

allow transmission_t self:process { getcap setcap signal_perms setsched setexec };
fs_rw_anon_inodefs_files(transmission_t)
fs_getattr_all_fs(transmission_t)
init_read_state(transmission_t)

# Allow reading system locale
miscfiles_read_localization(transmission_t)

# Allow reading random devices
dev_read_rand(transmission_t)
dev_read_urand(transmission_t)

# Allow configuring the dynamic linker
libs_exec_ldconfig(transmission_t)

# Allow reading system state
kernel_read_system_state(transmission_t)
kernel_read_network_state(transmission_t)
kernel_read_fs_sysctls(transmission_t)
dev_read_sysfs(transmission_t)

# Allow reading system network configuration
sysnet_read_config(transmission_t)

# Allow plex to use syslog/journald if configured to do so.
logging_send_syslog_msg(transmission_t)

# Allow transmission to use nsswitch
auth_use_nsswitch(transmission_t)

# Allow transmission to read quotas
allow transmission_t fs_t:filesystem quotaget;

#
# Tunables
#

# Allow transmission to manage public_content_rw_t labeled files and directories.
# By default, transmission can only read public_content_rw_t file and directories.
# This permission may be used for a user accessible watch directory.
gen_tunable(transmission_anon_write, false)

# Allows transmission to read systemwide cert store in /etc/pki.
gen_tunable(transmission_sys_cert_ro, false)

# Allow transmission to connect to http ports for reverse
# proxy setups.
gen_tunable(transmission_connect_http, false)

# Allow transmission to use local peer discovery protocol
gen_tunable(transmission_use_ldp, false)


#
# Tunable policies
#

tunable_policy(`transmission_anon_write',`
		miscfiles_manage_public_files(transmission_t)
')

tunable_policy(`transmission_sys_cert_ro',`
		miscfiles_read_generic_certs(transmission_t)
')

tunable_policy(`transmission_connect_http',`
		corenet_tcp_connect_http_port(transmission_t)
')

tunable_policy(`transmission_use_ldp',`
		allow transmission_t transmission_ldp_port_t:udp_socket name_bind;
')